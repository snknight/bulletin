## Cp E 421 - Embedded Systems

This is the repository that will contain all the material for the course, assignments, etc.
Important links will be found here along with links to outside videos and tutorials.


## Important Links:

| Description                                                                                                                       |   URL                                                                                             |
|-----------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------- |
|[Class Meeting Link](https://meet.jit.si/regular-meetings-of-university-of-mississippi-course-cpe421-embedded-systems)             | https://meet.jit.si/regular-meetings-of-university-of-mississippi-course-cpe421-embedded-systems  |
|[Syllabus · GitLab](https://gitlab.com/um-ece/courses/421/bulletin/-/blob/master/syllabus.md)                                      | https://gitlab.com/um-ece/courses/421/bulletin/-/blob/master/syllabus.md                          |
|[Schedule · GitLab](https://gitlab.com/um-ece/courses/421/bulletin/-/blob/master/schedule.md)                                      | https://gitlab.com/um-ece/courses/421/bulletin/-/blob/master/schedule.md                          |
|[Course Bulletin Board · GitLab](https://gitlab.com/um-ece/courses/421/bulletin)                                                   | https://gitlab.com/um-ece/courses/421/bulletin                                                    |
|[Assignments · GitLab](https://gitlab.com/um-ece/courses/421/assignments)                                                          | https://gitlab.com/um-ece/courses/421/assignments                                                 |
|[CpE 421 - Embedded Systems - YouTube (Playlist)](https://www.youtube.com/playlist?list=PLrJLEXbQpxazWfAO12c3OjfqASdRq6rYt)        | https://www.youtube.com/playlist?list=PLrJLEXbQpxazWfAO12c3OjfqASdRq6rYt                          |
|[CpE 421 - Embedded Systems - YouTube (live but with severe latency)](https://youtu.be/yKnmmtcYCCM)                                | https://youtu.be/yKnmmtcYCCM                                                                      |
|[Prof. Barry Muldrey - YouTube Channel](https://www.youtube.com/channel/UCh65j_usGlqMa8JW5Iea8ww)                                  | https://www.youtube.com/channel/UCh65j_usGlqMa8JW5Iea8ww                                          |
