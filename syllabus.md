---
author:
- 'Dr. Barry J. Muldrey'
date: Spring 2021
subtitle: Cp E 421
title: Embedded Systems
---

  ---------------- ----------------------------------
  Meeting Times:   Tuesday and Thursday 8:00a-9:15a
  Location:        Online Learning
  Credit Hours:    3
  Prerequisites    El E 351, El E 485/486
  Co-Requisites    None
  ---------------- ----------------------------------

Course Description {#course-description .unnumbered}
------------------

This course provides students exposure to advanced system-on-chip
programming. This course is meant to be natural extension of earlier
courses studying programming techniques for less-dense logic (i.e.
microcontrollers). In the course, we will advance from microcontroller
concepts to micro-system concepts. We will focus on the extremely
low-cost Allwinner A64 System-on-Chip. Students will study relevant
contemporary hardware/firmware/software concepts and methodologies, and
will gain practical skills. Each week, we will cover one theoretical
subject and one skill. We will try to get to the project ASAP, to ensure
adequate time for completion.

#### Topics Covered:

Version control tools, file systems, file display tools, toolchains,
hardware abstraction layer, compilation, cross-compilation, decompilers,
assemblers, disassemblers, bootstrapping, operating systems, Linux
kernel, RTOS kernels, and more.

#### Course Objectives:

-   Students will be able to debug SoC peripheral and OS interaction.

-   Students will be able to participate in the development of firmware.

#### Tool Usage:

-   VCS: Git

-   Languages: C, C++, python

-   Compilers: GCC, LLVM

-   SoC: Allwinner A64

Section Specifics {#section-specifics .unnumbered}
-----------------

#### Instructor Contact Info:

\

  --------------- ------------------------------------------------------------------------------------------------------------------
  Name:           Barry Muldrey
  Office:         Anderson 308
  Email:          <muldrey@olemiss.edu>
  Office phone:   \(662) 915-2623
  Office hours:   **\*by appointment\*:** I will make myself available for one hour after class for private and group discussions.
  --------------- ------------------------------------------------------------------------------------------------------------------

#### Schedule:

The meeting schedule is posted
[HERE](https://gitlab.com/um-ece/courses/421/student/-/blob/master/schedule.md)
in the main repo.

#### Mechanics:

In this term, we will be experimenting with the use of
[Gitlab.com](https://gitlab.com) as the basis for the course. You will
have to create an account there. It is free. For your reference, the
main page is <https://gitlab.com/um-ece/courses/421/student>.

#### Meetings:

For the reasons cataloged [HERE](https://hn.algolia.com/?q=zoom) and for
others, we will use the Jitsi platform for our class meetings. This is
the meeting link:\
<https://meet.jit.si/regular-meetings-of-university-of-mississippi-course-cpe421-embedded-systems>

#### Homework:

I will assign one homework per week. It will be posted by 12a (midnight)
Sunday, and will be due the following Sunday at 12a.

#### Reading:

There will be a reading assignment every week. You are expected to
complete the weeks' assignments by midnight on Sunday. Reading topics
are fair game for pop quizzes as early as Monday (might be an online
assignment).

#### Texts (optional):

-   Marilyn Wolf, "Computers as Components" (Fourth Edition), 2017, ISBN
    9780128053874 and here is a bunch of stuff and here is a bunch of
    stuff and here is a bunch of stuff
    <https://doi.org/10.1016/B978-0-12-805387-4.12001-1>

-   Edward A. Lee and Sanjit A. Seshia, Introduction to Embedded
    Systems, A Cyber-Physical Systems Approach, Second Edition, MIT
    Press, ISBN 978-0-262-53381-2, 2017.
    <https://ptolemy.berkeley.edu/books/leeseshia/>

#### Hardware Stewardship:

Each student will be given the following items for the purposes of
experimentation and development in the course. You are expected to be a
good steward of the hardware; every piece of hardware *must* be returned
*in good condition* before or on the last day of classes. If I do not
receive the hardware, I will not post your final grades. If you break or
lose your phone, order a replacement from Pine64.com; show me your
receipt, and I will give you one to use until the new one arrives.

1.  PinePhone (<https://Pine64.org>)

2.  USB-C charger

3.  64GB Micro-SD Card (High Endurance)

4.  Kingston High-Speed SD-Card Reader(+writer)

5.  Carrying case

#### Assessment:

Assessments may include discussion and participation assignments,
reading and other text-based assignments, reading quizzes, research
assignments, paraphrase and summary assignments, citation exercises, and
presentations, among other things.

#### Grade Breakdown:

\

  --------------- -----
  Participation   15%
  Assignments     10%
  Mid-term exam   20%
  Final Project   35%
  Final exam      20%
  --------------- -----

Communication Policies {#communication-policies .unnumbered}
----------------------

#### E-mail/Bb Messaging:

I will respond within 24 hours to emails which ask a question. I read
and respond to emails between 8:00 a.m. and 5:00 p.m. Mon.-Fri. I will
not read and respond to e-mails on weekends nor holidays on the academic
calendar. Whether I adhere to this or not, you must never **expect** me
to respond during these hours!

#### Telephone/Jitsi:

Students can also speak with me on the phone or on Jitsi during office
hours or at other times scheduled with me in advance.

#### Types of Questions:

Feel free to contact me with any questions about the course materials,
content, or assignments. However, if it is a grades question, please
first read the Frequently Asked Questions.

Course Policies {#course-policies .unnumbered}
---------------

#### Late Assignments + Extension Requests:

I do not accept late work. Extensions for individual assignments may be
granted, but must be requested at least **3 days in advance of the due
date.** Students will be granted a maximum of two extensions per
semester. To request an extension, tell me your full name, what course
and section number you are in, the specific name of the assignment, and
a proposed due date. (**NOTE:** Class work that requires class
participation, such as a discussion board or group work, cannot be
extended since it relies on the participation of other students. NOTE:
If a student uses an extension and sets a new due date and then wants to
push back the already extended due date, it will count as using another
extension.)

#### Extra Credit:

I do not give extra credit. I do, however, offer the opportunity to
receive feedback, revise, and resubmit assignments as often as you'd
like\... **up to 3 days prior to the due date**.

#### Written Assignments:

Written prose assignments should be submitted as pdflatex-generated PDF
files (using IEEEtran/acmconf/scrlttr/scrreprt styles as appropriate).
Less formal writing assignments may be submitted in Markdown if
permitted.

#### Attendance:

Since this is an online course, attendance is not tracked in the
traditional way. Not submitting assignments means that you did not
attend class. Instructors can view access reports for each student and
can see when the student logs in and what course content the student
accesses.

#### Assignment Submission:

Assignments will be submitted in a "cpe421-submissions" repo that each
user will fork from a provided template repository. For each assignment,
the user will enter the URL of their **final commit** into the course
submissions table.

1em **Syllabus Disclaimers: This syllabus is subject to change at the
discretion of the instructor. Changes will be posted to the main
repository in an updated syllabus and in an "announcements" file which
will also be posted to the course home page. The course outcomes, goals,
objectives, and student competencies will not change.**
